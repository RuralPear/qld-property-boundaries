Plugin Builder Results

Your plugin QldPropertyBoundaries was created in:
    /home/gp/Documents/Python/QldPropertyBoundaries/qld_property_boundaries

Your QGIS plugin directory is located at:
    /home/gp/.local/share/QGIS/QGIS3/profiles/default/python/plugins

What's Next:

  * Copy the entire directory containing your new plugin to the QGIS plugin
    directory

  * Run the tests (``make test``)

  * Test the plugin by enabling it in the QGIS plugin manager

  * Customize it by editing the implementation file: ``qld_property_boundaries.py``

  * You can use the Makefile to compile your Ui and resource files when
    you make changes. This requires GNU make (gmake)

For more information, see the PyQGIS Developer Cookbook at:
http://www.qgis.org/pyqgis-cookbook/index.html

(C) 2011-2018 GeoApt LLC - geoapt.com
