# WARNING: this plugin has been depreciated and is replaced by the [Qveg](https://gitlab.com/g.pattemore/qveg) plugin

# Queensland property boundaries

Loads Queensland property boundaries from lot/plan numbers
